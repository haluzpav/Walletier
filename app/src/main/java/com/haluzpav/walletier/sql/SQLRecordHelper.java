package com.haluzpav.walletier.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.haluzpav.walletier.data.Record;

/**
 * Provides access to the record table of the database.
 */
public class SQLRecordHelper extends SQLHelper {

    public SQLRecordHelper(Context context) {
        super(context);
    }

    /**
     * Gets specific record from database.
     * @param id id of record
     * @return Record
     */
    public Record getRecord(long id) {
        Cursor cursor = getReadableDatabase().rawQuery(String.format(SQLRecordString.GET_RECORD, id), null);
        if (!cursor.moveToFirst()) {
            return null;
        }
        Record rec = new Record(
                cursor.getLong(0),
                cursor.getDouble(1),
                cursor.getLong(2),
                cursor.getLong(3),
                cursor.getLong(4),
                cursor.getLong(5)
        );
        cursor.close();
        return rec;
    }

    /**
     * Adds record to database. Trigger in database automaticly geterates datetime of creation.
     * @param record Record to be added
     * @return id of newly added record
     */
    public long addRecord(Record record) {
        ContentValues values = new ContentValues();
        values.put(SQLRecordString.AMOUNT, record.getAmount());
        values.put(SQLRecordString.REF_CATEGORY, record.getReferenceCategory());
        values.put(SQLRecordString.DATETIME, record.getDatetime());
        return getWritableDatabase().insert(SQLRecordString.TABLE, null, values);
    }

    /**
     * Edits record with id provided in record with values also in record. Trigger in database
     * automaticly updates datetime of edition.
     * @param record Record containing id and values to be editted
     * @return number of affected records, should always be 1
     */
    public long editRecord(Record record) {
        ContentValues values = new ContentValues();
        values.put(SQLRecordString.AMOUNT, record.getAmount());
        values.put(SQLRecordString.REF_CATEGORY, record.getReferenceCategory());
        values.put(SQLRecordString.DATETIME, record.getDatetime());

        String whereClause = SQLRecordString.ID + " = ?";
        String[] whereArgs = {
                Long.toString(record.getId())
        };

        return getWritableDatabase().update(SQLRecordString.TABLE, values, whereClause, whereArgs);
    }

    /**
     * Removes specific record.
     * @param id id of record to be removed
     * @return number of affected records, should always be 1
     */
    public long removeRecord(long id) {
        String whereClause = SQLRecordString.ID + " = ?";
        String[] whereArgs = {
                Long.toString(id)
        };
        return getWritableDatabase().delete(SQLRecordString.TABLE, whereClause, whereArgs);
    }

    /**
     * Method to get command which can be used to get certain number of newest records. Not used
     * in current version, but the command took me a lot of effort to write, so I'll leave it here.
     * @param last count of records
     * @return String command
     */
    public String getLasts(long last) {
        return String.format(SQLRecordString.GET_LASTS, last);
    }

    /**
     * Method to get command for getting all records.
     * @return command
     */
    public String getAll() {
        return SQLRecordString.GET_ALL;
    }

    /**
     * Command to get sum of all records.
     * @return command
     */
    public String getSum() {
        return SQLRecordString.GET_SUM;
    }

    /**
     * Command to get sum of records in given time range. Time should be in unix time seconds.
     * @param from unix time in seconds, can be -1 for "now"
     * @param to unix time in seconds
     * @return command
     */
    public String getSum(long from, long to) {
        return from < 0 ?
                String.format(SQLRecordString.GET_SUM_INTERVAL, SQLString.GET_UNIXTIME, to) :
                String.format(SQLRecordString.GET_SUM_INTERVAL, from, to);
    }

    /**
     * So only these SQLHelper classes access SQLString classes.
     * @return array of columns used in "GET" commands
     */
    public String[] getSummaryColumns() {
        return SQLRecordString.SUMMARY_COLUMNS_FOR_ADAPTER;
    }

}
