package com.haluzpav.walletier.sql;

/**
 * Class containing values for category table of the database.
 */
abstract class SQLCategoryString extends SQLString {

    static final String TABLE = "category";
    private static final String TRIGGER_UPDATE = "trigger_update_" + TABLE;
    static final String NAME = "name";

    static final String TABLE_CREATE = "create table if not exists " +
            TABLE + "(" +
            ID + " integer primary key autoincrement, " +
            NAME + " text not null, " +
            CREATION_DATETIME + " integer default (" + GET_UNIXTIME + "), " +
            EDITTED_DATETIME + " integer default (" + GET_UNIXTIME + ")" +
            ")";
    static final String TABLE_DROP = "drop table if exists " + TABLE;

    /**
     * Changes edit_datetime when category is editted.
     */
    static final String TRIGGER_UPDATE_CREATE = String.format(
            "create trigger if not exists %s after update on %s begin update %s set %s = %s where %s = old.%s; end",
            TRIGGER_UPDATE, TABLE, TABLE, EDITTED_DATETIME, GET_UNIXTIME, ID, ID
    );

    /**
     * Command to get single specific category. Recomended usage: String.format(GET_CATEGORY, id),
     * where id is int variable with id of desired category.
     */
    static final String GET_CATEGORY = String.format(
            "select * from %s where %s = %s",
            TABLE, ID, "%d"
    );

    /**
     * Command to get number of records in which specific category is used.
     * Usage: String.format(GET_COUNT_CAT, categoryId).
     */
    static final String GET_COUNT_CAT = String.format(
            "select count(*) from %s where %s.%s = %s",
            SQLRecordString.TABLE, SQLRecordString.TABLE, SQLRecordString.REF_CATEGORY, "%s"
    );

    /**
     * Names of columns to pass to adapter which interprets received Cursor.
     */
    static final String[] LIST_COLUMNS_FOR_ADAPTER = new String[]{
            ID, NAME, "usage"
    };

    /**
     * Names of columns that are actually read from database including names of tables (to avoid
     * confusion in column names in different tables) and also renaming them (to LIST_COLUMNS_FOR_ADAPTER),
     * because adapter throws warnings if column name include name of table.
     */
    private static final String[] LIST_COLUMNS_FOR_COMMAND = new String[]{
            String.format(COLUMN_MASK, TABLE, ID, LIST_COLUMNS_FOR_ADAPTER[0]),
            String.format(COLUMN_MASK, TABLE, NAME, LIST_COLUMNS_FOR_ADAPTER[1]),
            String.format("(%s) as %s",
                    String.format(GET_COUNT_CAT, TABLE + "." + ID),
                    LIST_COLUMNS_FOR_ADAPTER[2])
    };

    /**
     * Command to get all categories.
     */
    static final String GET_ALL = String.format(
            "select %s from %s",
            arrayToString(LIST_COLUMNS_FOR_COMMAND), TABLE
    );

}
