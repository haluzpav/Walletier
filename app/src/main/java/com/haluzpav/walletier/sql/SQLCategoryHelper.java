package com.haluzpav.walletier.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.haluzpav.walletier.data.Category;

/**
 * Provides access to the category table of the database.
 */
public class SQLCategoryHelper extends SQLHelper {

    public SQLCategoryHelper(Context context) {
        super(context);
    }

    /**
     * Gets specific category from database.
     * @param id id of category
     * @return Category
     */
    public Category getCategory(long id) {
        Cursor cursor = getReadableDatabase().rawQuery(String.format(SQLCategoryString.GET_CATEGORY, id), null);
        if (!cursor.moveToFirst()) {
            return null;
        }
        Category cat = new Category(
                cursor.getLong(0),
                cursor.getString(1),
                cursor.getLong(2),
                cursor.getLong(3)
        );
        cursor.close();
        return cat;
    }

    /**
     * Adds category to database. Trigger in database automaticly geterates datetime of creation.
     * @param category Category to be added
     * @return id of newly added category
     */
    public long addCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put(SQLCategoryString.NAME, category.getName());
        return getWritableDatabase().insert(SQLCategoryString.TABLE, null, values);
    }

    /**
     * Edits category with id provided in category with values also in category. Trigger in database
     * automaticly updates datetime of edition.
     * @param category Category containing id and values to be editted
     * @return number of affected categories, should always be 1
     */
    public long editCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put(SQLCategoryString.NAME, category.getName());

        String whereClause = SQLCategoryString.ID + " = ?";
        String[] whereArgs = {
                Long.toString(category.getId())
        };

        return getWritableDatabase().update(SQLCategoryString.TABLE, values, whereClause, whereArgs);
    }

    /**
     * Removes specific category.
     * @param id id of category to be removed
     * @return number of affected categories, should always be 1
     */
    public long removeCategory(long id) {
        String whereClause = SQLCategoryString.ID + " = ?";
        String[] whereArgs = {
                Long.toString(id)
        };
        return getWritableDatabase().delete(SQLCategoryString.TABLE, whereClause, whereArgs);
    }

    /**
     * Gets all categories also with number of usage in records. Used for synchrononized
     * reading from database.
     * @return Cursor
     */
    public Cursor getAll() {
        return getReadableDatabase().rawQuery(SQLCategoryString.GET_ALL, null);
    }

    /**
     * Returns number of records in which category with given id is used.
     * @param id id of category
     * @return number of records with this category
     */
    public long getCount(long id) {
        Cursor cursor = getCursor(String.format(SQLCategoryString.GET_COUNT_CAT, id));
        if (!cursor.moveToFirst()) {
            return -1;
        }
        return cursor.getLong(0);
    }

    /**
     * So only these SQLHelper classes access SQLString classes.
     * @return array of columns used in GET_ALL command
     */
    public String[] getColumns() {
        return SQLCategoryString.LIST_COLUMNS_FOR_ADAPTER;
    }

    /**
     * Command to get all categories.
     * @return command
     */
    public String getAllCommand() {
        return SQLCategoryString.GET_ALL;
    }

}
