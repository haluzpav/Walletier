package com.haluzpav.walletier.sql;

/**
 * Class containing values for record table of the database.
 */
abstract class SQLRecordString extends SQLString {

    static final String TABLE = "record";
    private static final String TRIGGER_UPDATE = "trigger_update_" + TABLE;
    static final String AMOUNT = "amount";
    static final String REF_CATEGORY = "ref_category";
    static final String DATETIME = "datetime";

    static final String TABLE_CREATE = "create table if not exists " +
            TABLE + "(" +
            ID + " integer primary key autoincrement, " +
            AMOUNT + " real not null, " +
            REF_CATEGORY + " integer not null references " + SQLCategoryString.TABLE + "(" + SQLCategoryString.ID + "), " +
            DATETIME + " integer not null, " +
            CREATION_DATETIME + " integer default (" + GET_UNIXTIME + "), " +
            EDITTED_DATETIME + " integer default (" + GET_UNIXTIME + ")" +
            ")";
    static final String TABLE_DROP = "drop table if exists " + TABLE;

    /**
     * Changes edit_datetime when record is editted.
     */
    static final String TRIGGER_UPDATE_CREATE = String.format(
            "create trigger if not exists %s after update on %s begin update %s set %s = %s where %s = old.%s; end",
            TRIGGER_UPDATE, TABLE, TABLE, EDITTED_DATETIME, GET_UNIXTIME, ID, ID
    );

    /**
     * Command to get single specific record. Recomended usage: String.format(GET_RECORD, id),
     * where id is int variable with id of desired record.
     */
    static final String GET_RECORD = String.format(
            "select * from %s where %s = %s",
            TABLE, ID, "%d"
    );

    /**
     * Names of columns to pass to adapter, which interprets received Cursor.
     */
    static final String[] SUMMARY_COLUMNS_FOR_ADAPTER = new String[]{
            ID, AMOUNT, "category", DATETIME
    };

    /**
     * Names of columns that are actually read from database including name of table (to avoid
     * confusion of column names in different tables) and also renaming them (to SUMMARY_COLUMNS_FOR_ADAPTER),
     * because adapter throws warnings if column names include name of table.
     */
    private static final String[] SUMMARY_COLUMNS_FOR_COMMAND = new String[]{
            String.format(COLUMN_MASK, TABLE, ID, SUMMARY_COLUMNS_FOR_ADAPTER[0]),
            String.format(COLUMN_MASK, TABLE, AMOUNT, SUMMARY_COLUMNS_FOR_ADAPTER[1]),
            String.format(COLUMN_MASK, SQLCategoryString.TABLE, SQLCategoryString.NAME, SUMMARY_COLUMNS_FOR_ADAPTER[2]),
            String.format(COLUMN_MASK, TABLE, DATETIME, SUMMARY_COLUMNS_FOR_ADAPTER[3]),
    };

    /**
     * Command to get specific count of last records sorted by newest.
     */
    static final String GET_LASTS = String.format(
            "select %s from %s join %s where %s = %s order by %s desc limit %s",
            arrayToString(SUMMARY_COLUMNS_FOR_COMMAND), TABLE, SQLCategoryString.TABLE,
            TABLE + "." + REF_CATEGORY, SQLCategoryString.TABLE + "." + SQLCategoryString.ID,
            DATETIME, "%d"
    );

    /**
     * Command to get all records sorted by newest.
     */
    static final String GET_ALL = String.format(
            "select %s from %s join %s where %s = %s order by %s desc",
            arrayToString(SUMMARY_COLUMNS_FOR_COMMAND), TABLE, SQLCategoryString.TABLE,
            TABLE + "." + REF_CATEGORY, SQLCategoryString.TABLE + "." + SQLCategoryString.ID,
            DATETIME
    );

    /**
     * Command to get sum of amount column of all records.
     */
    static final String GET_SUM = String.format(
            "select sum(%s) from %s",
            AMOUNT, TABLE
    );

    /**
     * Command to get sum of amount column of records in specific time range. Usage:
     * String.format(GET_SUM_INTERVAL, from, to), where from and to are int values representing
     * unix time in seconds.
     */
    static final String GET_SUM_INTERVAL = String.format(
            "select sum(num) from (select %s as num from %s where %s - %s < %s)",
            AMOUNT, TABLE, "%s", DATETIME, "%d"
    );

}
