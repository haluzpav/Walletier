package com.haluzpav.walletier.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Provides access to the database.
 */
public abstract class SQLHelper extends SQLiteOpenHelper {

    SQLHelper(Context context) {
        super(context, SQLString.DATABASE_NAME, null, SQLString.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLString.ENABLE_FOREIGN_KEYS);
        db.execSQL(SQLCategoryString.TABLE_CREATE);
        db.execSQL(SQLRecordString.TABLE_CREATE);
        db.execSQL(SQLRecordString.TRIGGER_UPDATE_CREATE);
        db.execSQL(SQLCategoryString.TRIGGER_UPDATE_CREATE);
    }

    public void onOpen(SQLiteDatabase db) {
        db.execSQL(SQLString.ENABLE_FOREIGN_KEYS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQLRecordString.TABLE_DROP);
        db.execSQL(SQLCategoryString.TABLE_DROP);
        onCreate(db);
    }

    /**
     * Should be used only in non-UI thread as getting Cursor can take a lot of time.
     * @param command command defining what values get from database
     * @return Cursor with values
     */
    public Cursor getCursor(String command) {
        return getReadableDatabase().rawQuery(command, null);
    }

}
