package com.haluzpav.walletier.sql;

/**
 * Class which stores basic values for database and also some values which are used in all tables.
 * These values and values of classes extending this class are protected or package-private,
 * so they can be accessed only between themselves or from Helpers which are using them
 * to control the database.
 */
abstract class SQLString {

    static final String DATABASE_NAME = "walletier.db";
    static final int DATABASE_VERSION = 1;

    /**
     * Command to enable references from one table to another.
     */
    static final String ENABLE_FOREIGN_KEYS = "pragma foreign_keys = on";

    static final String ID = "_id";
    static final String CREATION_DATETIME = "datetime_created";
    static final String EDITTED_DATETIME = "datetime_editted";

    /**
     * Function to get current time in seconds.
     */
    static final String GET_UNIXTIME = "strftime('%s', 'now')";

    /**
     * Mask used when columns from table are being selected.
     * Usage: "TABLE.COLUMN as COLUMN_IN_ADAPTER"
     */
    static final String COLUMN_MASK = "%s.%s as %s";

    static String arrayToString(String[] array) {
        String ret = "";
        for (int i = 0; i < array.length; i++) {
            ret += array[i];
            if (i != array.length - 1) {
                ret += ", ";
            }
        }
        return ret;
    }

}
