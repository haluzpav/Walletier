package com.haluzpav.walletier.exception;

/**
 * Exception usually thrown when some input is missing.
 */
public class EmptyInputException extends Exception {

    public EmptyInputException(String detailMessage) {
        super(detailMessage);
    }

}
