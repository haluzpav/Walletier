package com.haluzpav.walletier.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import com.haluzpav.walletier.R;

/**
 * Dialog used for editting category
 */
public class EditCategoryDialogFragment extends CategoryDialogFragment {

    @Override
    public void onStart() {
        super.onStart();
        editText.setHint(R.string.dialog_edit_category_edittext_hint);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = createGenericBuilder();
        builder.setTitle(R.string.dialog_edit_category_title);
        return builder.create();
    }

}
