package com.haluzpav.walletier.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.haluzpav.walletier.R;

/**
 * DialogFragment which is used to show dialog asking for confirmation of deleting record
 * or category from database. Specific actions are defined in listener.
 */
public class AskToDeleteDialogFragment extends DialogFragment {

    private DialogListener listener;
    private int messageId;

    /**
     * Creates dialog using a Builder.
     * @param savedInstanceState saved instance state
     * @return dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(messageId == 0 ? R.string.dialog_delete_question_default : messageId);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (listener != null) {
                    listener.onClickConfirm(dialog, id);
                }
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                if (listener != null) {
                    listener.onClickCancel(dialog, id);
                }
            }
        });
        return builder.create();
    }

    /**
     * Sets listener. Should be called before this.show(), otherwise nothing will happen
     * when click on buttons.
     * @param listener listener to be set
     */
    public void setListener(DialogListener listener) {
        this.listener = listener;
    }

    /**
     * Sets message to be displayed above buttons.
     * @param messageId id of message (usually comes from R.string)
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * Defines methods for listener.
     */
    public interface DialogListener {
        /**
         * Used when dialog is confirmed.
         * @param dialog dialog
         * @param id id of button
         */
        void onClickConfirm(DialogInterface dialog, int id);

        /**
         * Used when dialog is canceled.
         * @param dialog dialog
         * @param id id of button
         */
        void onClickCancel(DialogInterface dialog, int id);
    }

}
