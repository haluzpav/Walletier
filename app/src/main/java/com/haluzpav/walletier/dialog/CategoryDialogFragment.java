package com.haluzpav.walletier.dialog;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.data.Category;
import com.haluzpav.walletier.exception.EmptyInputException;

/**
 * Basic abstract fragment for creating dialogs which work create/edit categories.
 */
abstract public class CategoryDialogFragment extends DialogFragment {

    private CategoryDialogListener externalListener;
    private Category category;
    EditText editText;

    /**
     * Sets listeners to buttons if present. It's not done in Builder because I needed to bypass
     * dissmissing a dialog. It should stay open when confirmed and values are invalid.
     */
    @Override
    public void onStart() {
        super.onStart();
        initViews();
        fillViews();

        if (externalListener == null) {
            throw new RuntimeException("No external listener on dialog's buttons");
        }
        final AlertDialog alertDialog = (AlertDialog) getDialog();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    fillCategory();
                    externalListener.onClickConfirm(alertDialog, category);
                    dismiss();
                } catch (EmptyInputException e) {
                    Toast.makeText(alertDialog.getOwnerActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                externalListener.onClickCancel(alertDialog);
                dismiss();
            }
        });
    }

    /**
     * Sets category to be shown on start.
     * @param category category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * Sets listener which should have defined actions on clicking buttons
     * @param listener listener
     */
    public void setListener(CategoryDialogListener listener) {
        this.externalListener = listener;
    }

    /**
     * Fills Category with given values.
     * @throws EmptyInputException
     */
    private void fillCategory() throws EmptyInputException {
        String name = editText.getText().toString();
        if (name.isEmpty()) {
            throw new EmptyInputException("Name of the category can't be empty!");
        } else {
            category.setName(name);
        }
    }

    /**
     * Gets Views from layout, so it's easier to work with them.
     */
    private void initViews() {
        editText = (EditText) getDialog().findViewById(R.id.dialog_category_edittext);
    }

    /**
     * Fills Views with given Category.
     */
    private void fillViews() {
        if (category != null) {
            editText.setText(category.getName());
        }
    }

    /**
     * Creates generic Builder which is further expanded in implementations.
     * @return generic Builder
     */
    AlertDialog.Builder createGenericBuilder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(getActivity().getLayoutInflater().inflate(R.layout.dialog_category, null));
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setNegativeButton(android.R.string.cancel, null);
        return builder;
    }

}
