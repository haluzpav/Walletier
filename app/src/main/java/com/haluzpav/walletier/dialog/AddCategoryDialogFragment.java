package com.haluzpav.walletier.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.data.Category;

/**
 * Dialog used to create new category.
 */
public class AddCategoryDialogFragment extends CategoryDialogFragment {

    /**
     * Alters super's dialog.
     */
    @Override
    public void onStart() {
        super.onStart();
        editText.setHint(R.string.dialog_create_category_edittext_hint);
    }

    /**
     * Creates a dialog.
     * @param savedInstanceState saved instance state
     * @return dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.setCategory(new Category());
        AlertDialog.Builder builder = createGenericBuilder();
        builder.setTitle(R.string.dialog_create_category_title);
        return builder.create();
    }

}
