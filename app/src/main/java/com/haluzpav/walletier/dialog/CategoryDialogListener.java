package com.haluzpav.walletier.dialog;

import android.content.DialogInterface;

import com.haluzpav.walletier.data.AbstractEntry;

/**
 * Defines methods of listener for dialog adding/editting categories
 */
public interface CategoryDialogListener {

    /**
     * Called when clicked on confirm button.
     * @param dialog dialog
     * @param abstractEntry in this case, it should be just Category
     */
    void onClickConfirm(DialogInterface dialog, AbstractEntry abstractEntry);
    void onClickCancel(DialogInterface dialog);

}
