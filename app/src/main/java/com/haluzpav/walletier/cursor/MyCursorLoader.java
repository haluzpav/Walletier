package com.haluzpav.walletier.cursor;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;

import com.haluzpav.walletier.sql.SQLHelper;

/**
 * My simplified version of CursorLoader. The main difference, which is needed, is using
 * method rawQuery instead of query on the database. RawQuery allows more complex queries.
 * It is also supposed that it will run in another thread.
 */
class MyCursorLoader extends AsyncTaskLoader<Cursor> {

    private final SQLHelper mSQLHelper;
    private final String mCommand;

    private Cursor mCursor;

    public MyCursorLoader(Context context, SQLHelper sqlHelper, String command) {
        super(context);
        mSQLHelper = sqlHelper;
        mCommand = command;
    }

    /**
     * The most important method I needed to modify. Provides some basic check of returned Cursor.
     * @return Cursor with values from database.
     */
    @Override
    public Cursor loadInBackground() {
        Cursor cursor = mSQLHelper.getCursor(mCommand);
        if (cursor != null) {
            try {
                cursor.getCount();
            } catch (RuntimeException ex) {
                cursor.close();
                throw ex;
            }
        }
        return cursor;
    }

    @Override
    public void deliverResult(Cursor cursor) {
        if (isReset()) {
            if (cursor != null) {
                cursor.close();
            }
            return;
        }
        Cursor oldCursor = mCursor;
        mCursor = cursor;

        if (isStarted()) {
            super.deliverResult(cursor);
        }

        if (oldCursor != null && oldCursor != cursor && !oldCursor.isClosed()) {
            oldCursor.close();
        }
    }

    @Override
    protected void onStartLoading() {
        if (mCursor != null) {
            deliverResult(mCursor);
        }
        if (takeContentChanged() || mCursor == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onCanceled(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        mCursor = null;
    }

}