package com.haluzpav.walletier.cursor;

import android.app.LoaderManager;
import android.content.Context;
import android.database.Cursor;

import com.haluzpav.walletier.sql.SQLHelper;
/**
 * It's like a worker, which starts getting informations from database in another thread,
 * and then puts them into eg. TextView.
 */
abstract class CursorHandler implements LoaderManager.LoaderCallbacks<Cursor> {

    final Context context;
    final SQLHelper sqlHelper;
    final String command;
    private static int id = 0;

    CursorHandler(Context context, SQLHelper sqlHelper, String command) {
        this.context = context;
        this.sqlHelper = sqlHelper;
        this.command = command;
    }

    /**
     * Initializes Loader which then permorms actions in another thread.
     * @param loaderManager usually provided from activity, which calls this method
     */
    public void startLoader(LoaderManager loaderManager) {
        loaderManager.initLoader(nextId(), null, this);
    }

    private static synchronized int nextId() {
        return id++;
    }

}
