package com.haluzpav.walletier.cursor;

import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.SimpleCursorAdapter;

import com.haluzpav.walletier.sql.SQLHelper;

/**
 * Gets single value from database in seperate thread and then puts it into provided CursorAdapter.
 */
public class CursorAdapterHandler extends CursorHandler {

    private final SimpleCursorAdapter adapter;

    /**
     * Constructor.
     * @param context Usually a Activity which creates this.
     * @param sqlHelper To access database.
     * @param command Command which is used to get values from database.
     * @param adapter Where to put the values.
     */
    public CursorAdapterHandler(Context context, SQLHelper sqlHelper, String command, SimpleCursorAdapter adapter) {
        super(context, sqlHelper, command);
        this.adapter = adapter;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(context, sqlHelper, command);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.changeCursor(null);
    }

}
