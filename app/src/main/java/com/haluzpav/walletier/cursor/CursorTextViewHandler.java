package com.haluzpav.walletier.cursor;

import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.haluzpav.walletier.sql.SQLHelper;

/**
 * Gets single value from database in seperate thread and then puts it into provided TextView.
 */
public class CursorTextViewHandler extends CursorHandler {

    private final TextView textView;
    private final String format;

    /**
     * Constructor.
     * @param context Usually a Activity which calls this.
     * @param sqlHelper To access database.
     * @param command Command which is used to get value from database.
     * @param textView Where to put the value.
     * @param format How to format the value.
     */
    public CursorTextViewHandler(Context context, SQLHelper sqlHelper, String command, TextView textView, String format) {
        super(context, sqlHelper, command);
        this.textView = textView;
        this.format = format;
    }

    private double getSingleDouble(Cursor cursor) {
        if (!cursor.moveToFirst()) {
            return 0;
        }
        return cursor.getFloat(0);
    }

    /**
     * Creates new Loader. Called from LoaderManager.initLoader().
     * @param id id of thread
     * @param args some data, not used
     * @return Loader
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(context, sqlHelper, command);
    }

    /**
     * Called when Loader finished.
     * @param loader Loader
     * @param data Loaded Cursor with informations.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        textView.setText(String.format(format, getSingleDouble(data)));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        textView.setText(null);
    }

}
