package com.haluzpav.walletier.data;

/**
 * Represents one record.
 */
public class Record extends AbstractEntry {

    private double amount;
    private long referenceCategory;
    private long datetime;

    public Record(){
        super();
    }

    public Record(long id, double amount, long referenceCategory, long datetime, long datetimeCreated, long datetimeEditted) {
        super(id, datetimeCreated, datetimeEditted);
        this.amount = amount;
        this.referenceCategory = referenceCategory;
        this.datetime = datetime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getReferenceCategory() {
        return referenceCategory;
    }

    public void setReferenceCategory(long referenceCategory) {
        this.referenceCategory = referenceCategory;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

}
