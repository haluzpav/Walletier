package com.haluzpav.walletier.data;

/**
 * Represents one category.
 */
public class Category extends AbstractEntry {

    private String name;

    public Category(){
        super();
    }

    public Category(long id, String name, long datetimeCreated, long datetimeEditted) {
        super(id, datetimeCreated, datetimeEditted);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
