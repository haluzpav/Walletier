package com.haluzpav.walletier.data;

/**
 * Abstract class representing basic structure of records/categories in database.
 */
abstract public class AbstractEntry {

    private final long id;
    private final long datetimeCreated;
    private final long datetimeEditted;

    /**
     * This constructor is used only when creating new record/category. ID and datetimes
     * are irrelevant as database creates them on its own.
     */
    AbstractEntry() {
        this.id = -1;
        this.datetimeCreated = -1;
        this.datetimeEditted = -1;
    }

    /**
     * Constructor meant for reading records from database.
     * @param id id
     * @param datetimeCreated datetime (in seconds) of creation
     * @param datetimeEditted datetime of last edit
     */
    AbstractEntry(long id, long datetimeCreated, long datetimeEditted) {
        this.id = id;
        this.datetimeCreated = datetimeCreated;
        this.datetimeEditted = datetimeEditted;
    }

    public long getId() {
        return id;
    }

    public long getDatetimeCreated() {
        return datetimeCreated;
    }

    public long getDatetimeEditted() {
        return datetimeEditted;
    }

}
