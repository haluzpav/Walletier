package com.haluzpav.walletier.activity;

import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.misc.ValuesHelper;

public class RecordEditActivity extends RecordActivity {

    @Override
    protected void onStart() {
        super.onStart();
        long recordId = getIntent().getLongExtra(ValuesHelper.EXTRA_RECORD_ID, 0);
        record = sqlRecordHelper.getRecord(recordId);
        fillViews(record);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.findItem(R.id.action_save_record).setIcon(android.R.drawable.ic_menu_edit);
        return true;
    }

    /**
     * Called when click on confirmation button. Gets values from Views and then saves them
     * to database.
     * @return number of editted records (should be 1) or -1 if values were in bad format
     */
    @Override
    protected long confirm() {
        Log.d(TAG, "confirmed");
        if (!fillRecord()) {
            Log.e(TAG, "confirmation unsuccessful");
            return -1;
        }
        long editted = sqlRecordHelper.editRecord(record);
        if (editted == 1) {
            Log.i(TAG, "record successfully editted");
            Toast.makeText(this, R.string.toast_record_editted_success, Toast.LENGTH_SHORT).show();
        } else if (editted > 1) {
            Log.w(TAG, "editted more records than one");
            Toast.makeText(this, R.string.toast_record_editted_fail, Toast.LENGTH_SHORT).show();
        } else if (editted < 1) {
            Log.w(TAG, "editted less records than one");
            Toast.makeText(this, R.string.toast_record_editted_fail, Toast.LENGTH_SHORT).show();
        }
        return editted;
    }

}
