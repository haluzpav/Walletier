package com.haluzpav.walletier.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.cursor.CursorAdapterHandler;
import com.haluzpav.walletier.cursor.CursorTextViewHandler;
import com.haluzpav.walletier.sql.SQLRecordHelper;
import com.haluzpav.walletier.dialog.AskToDeleteDialogFragment;
import com.haluzpav.walletier.misc.MyViewBinder;
import com.haluzpav.walletier.misc.ValuesHelper;

/**
 * Main activity which is shown at start of app. It contains current balance on account and
 * list of records.
 */
public class SummaryActivity extends Activity {

    private SQLRecordHelper sqlRecordHelper;
    private static final String TAG = "SummaryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        sqlRecordHelper = new SQLRecordHelper(this);
    }

    protected void onResume() {
        Log.i(TAG, "resuming");
        super.onResume();
        fillHeader();
        fillLastRecords();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_start_add_record:
                openAddRecord();
                return true;
            case R.id.action_category:
                openCategory();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Starts another activity for adding records.
     */
    private void openAddRecord() {
        Intent intent = new Intent(this, RecordAddActivity.class);
        startActivity(intent);
    }

    /**
     * Starts activity for managing categories.
     */
    private void openCategory() {
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }

    /**
     * Starts activity to edit record. Basicly same as activity for adding records, but
     * fields will be already filled with values from database.
     * @param id id of record
     */
    private void openEditRecord(long id) {
        Intent intent = new Intent(this, RecordEditActivity.class);
        intent.putExtra(ValuesHelper.EXTRA_RECORD_ID, id);
        startActivity(intent);
    }

    /**
     * Creates dialog which demands confirmation of deleting record.
     * @param id id of record
     */
    private void askToRemoveRecord(final long id) {
        Log.d(TAG, "creating dialog to confirm deletion of record");
        AskToDeleteDialogFragment askToDeleteDialogFragment = new AskToDeleteDialogFragment();
        askToDeleteDialogFragment.setListener(new AskToDeleteDialogFragment.DialogListener() {
            @Override
            public void onClickCancel(DialogInterface dialog, int buttonId) {
                Log.d(TAG, "record deletion canceled");
            }

            @Override
            public void onClickConfirm(DialogInterface dialog, int buttonId) {
                Log.d(TAG, "record deletion confirmed, deleting...");
                sqlRecordHelper.removeRecord(id);
                Log.i(TAG, "record deleted");
                Toast.makeText(((Dialog) dialog).getOwnerActivity(), R.string.toast_record_deleted, Toast.LENGTH_SHORT).show();
                fillHeader();
                fillLastRecords();
            }
        });
        askToDeleteDialogFragment.setMessageId(R.string.dialog_delete_question_record);
        askToDeleteDialogFragment.show(getFragmentManager(), "delete record");
    }

    /**
     * Starts loaders (in another thread) which fill textViews in header.
     */
    private void fillHeader() {
        Log.i(TAG, "filling header");

        TextView textViewSumAll = (TextView) findViewById(R.id.current_balance);
        new CursorTextViewHandler(
                this,
                sqlRecordHelper,
                sqlRecordHelper.getSum(),
                textViewSumAll,
                ValuesHelper.PRICE_FORMAT
        ).startLoader(getLoaderManager());

        TextView textViewSumMonth = (TextView) findViewById(R.id.month_change);
        new CursorTextViewHandler(
                this,
                sqlRecordHelper,
                sqlRecordHelper.getSum(-1, ValuesHelper.SECONDS_30_DAYS),
                textViewSumMonth,
                ValuesHelper.PRICE_FORMAT
        ).startLoader(getLoaderManager());
    }

    /**
     * Starts loader (in another thread) which fills listView with records.
     */
    private void fillLastRecords() {
        Log.i(TAG, "filling list of records");

        int[] textViews = new int[]{
                0,
                R.id.entry_record_summary_amount,
                R.id.entry_record_summary_category,
                R.id.entry_record_summary_datetime
        };

        ListView listView = (ListView) findViewById(R.id.listView);

        LinearLayout progressBar = (LinearLayout) findViewById(R.id.progressbar_layout);
        listView.setEmptyView(progressBar);

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                this,
                R.layout.entry_record_summary, null,
                sqlRecordHelper.getSummaryColumns(), textViews
        );
        cursorAdapter.setViewBinder(new MyViewBinder());
        listView.setAdapter(cursorAdapter);
        new CursorAdapterHandler(
                this,
                sqlRecordHelper,
                sqlRecordHelper.getAll(),
                cursorAdapter
        ).startLoader(getLoaderManager());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "clicked: record id " + id);
                openEditRecord(id);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "long-clicked: record id " + id);
                askToRemoveRecord(id);
                return true;
            }
        });
    }

}
