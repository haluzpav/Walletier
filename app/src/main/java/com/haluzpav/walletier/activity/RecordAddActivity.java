package com.haluzpav.walletier.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.data.Record;

/**
 * Activity for adding new records to database
 */
public class RecordAddActivity extends RecordActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        record = new Record();
    }

    /**
     * Called when click on confirmation button. Gets values from Views and then saves them
     * to database.
     * @return id of newly created record or -1 if values were in bad format
     */
    @Override
    protected long confirm() {
        Log.d(TAG, "confirmed");
        if (!fillRecord()) {
            Log.e(TAG, "confirmation unsuccessful");
            return -1;
        }
        long added = sqlRecordHelper.addRecord(record);
        Log.i(TAG, "new record added with id " + added);
        Toast.makeText(this, R.string.toast_record_added, Toast.LENGTH_SHORT).show();
        return added;
    }

}
