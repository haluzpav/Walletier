package com.haluzpav.walletier.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.data.AbstractEntry;
import com.haluzpav.walletier.dialog.AskToDeleteDialogFragment;
import com.haluzpav.walletier.cursor.CursorAdapterHandler;
import com.haluzpav.walletier.dialog.CategoryDialogFragment;
import com.haluzpav.walletier.dialog.AddCategoryDialogFragment;
import com.haluzpav.walletier.dialog.CategoryDialogListener;
import com.haluzpav.walletier.dialog.EditCategoryDialogFragment;
import com.haluzpav.walletier.data.Category;
import com.haluzpav.walletier.sql.SQLCategoryHelper;
import com.haluzpav.walletier.misc.MyViewBinder;

/**
 * Activity for managing categories. For adding/editting single categories, it shows a dialog.
 */
public class CategoryActivity extends Activity {

    private SQLCategoryHelper sqlCategoryHelper;
    private static final String TAG = "CategoryActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        sqlCategoryHelper = new SQLCategoryHelper(this);
    }

    protected void onResume() {
        super.onResume();
        fillList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_category:
                addCategory();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fills ListView with categories.
     */
    private void fillList() {
        Log.d(TAG, "filling list of categories");
        int[] textViews = new int[]{
                0,
                R.id.entry_category_name,
                R.id.entry_category_usage
        };

        ListView listView = (ListView) findViewById(R.id.listview);

        LinearLayout progressBar = (LinearLayout) findViewById(R.id.progressbar_layout);
        listView.setEmptyView(progressBar);

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                this,
                R.layout.entry_category, null,
                sqlCategoryHelper.getColumns(), textViews
        );
        cursorAdapter.setViewBinder(new MyViewBinder());
        listView.setAdapter(cursorAdapter);
        new CursorAdapterHandler(
                this,
                sqlCategoryHelper,
                sqlCategoryHelper.getAllCommand(),
                cursorAdapter
        ).startLoader(getLoaderManager());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "clicked: category id " + id);
                editCategory(id);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "long-clicked: category id " + id);
                askToRemoveCategory(id);
                return true;
            }
        });
    }

    /**
     * Opens dialog to add a category.
     */
    private void addCategory() {
        Log.d(TAG, "creating dialog to add category");
        CategoryDialogFragment addCategoryDialogFragment = new AddCategoryDialogFragment();
        addCategoryDialogFragment.setListener(new CategoryDialogListener() {
            @Override
            public void onClickCancel(DialogInterface dialog) {
                Log.d(TAG, "adding of new category canceled");
            }

            @Override
            public void onClickConfirm(DialogInterface dialog, AbstractEntry category) {
                Log.d(TAG, "adding new category");
                long added = sqlCategoryHelper.addCategory((Category) category);
                Log.i(TAG, "added category with id " + added);
                Toast.makeText(((Dialog) dialog).getOwnerActivity(), R.string.toast_category_added, Toast.LENGTH_SHORT).show();
                fillList();
            }
        });
        addCategoryDialogFragment.show(getFragmentManager(), "create category");
    }

    /**
     * Opens dialog to edit a category with specific id.
     * @param id id of category
     */
    private void editCategory(final long id) {
        Log.d(TAG, "creating dialog to edit category id " + id);
        CategoryDialogFragment editCategoryDialogFragment = new EditCategoryDialogFragment();
        editCategoryDialogFragment.setListener(new CategoryDialogListener() {
            @Override
            public void onClickCancel(DialogInterface dialog) {
                Log.d(TAG, "editting of category canceled");
            }

            @Override
            public void onClickConfirm(DialogInterface dialog, AbstractEntry category) {
                Log.d(TAG, "editting category");
                long editted = sqlCategoryHelper.editCategory((Category) category);
                Log.i(TAG, "number of editted categories: " + editted);
                Toast.makeText(((Dialog) dialog).getOwnerActivity(), R.string.toast_category_editted, Toast.LENGTH_SHORT).show();
                fillList();
            }
        });
        Category category = sqlCategoryHelper.getCategory(id);
        editCategoryDialogFragment.setCategory(category);
        editCategoryDialogFragment.show(getFragmentManager(), "edit category");
    }

    /**
     * Opens a dialog to confirm deleting of category with specific id.
     * @param id id of category to delete
     */
    private void askToRemoveCategory(final long id) {
        Log.d(TAG, "creating dialog to confirm deletion of category");
        final AskToDeleteDialogFragment askToDeleteDialogFragment = new AskToDeleteDialogFragment();
        askToDeleteDialogFragment.setListener(new AskToDeleteDialogFragment.DialogListener() {
            @Override
            public void onClickCancel(DialogInterface dialog, int buttonId) {
                Log.d(TAG, "category deletion canceled");
            }

            @Override
            public void onClickConfirm(DialogInterface dialog, int buttonId) {
                Log.d(TAG, "category deletion confirmed, deleting...");
                long usage = sqlCategoryHelper.getCount(id);
                if (usage > 0) {
                    Log.w(TAG, "unable to delete category, used in " + usage + " records");
                    Toast.makeText(((Dialog) dialog).getOwnerActivity(),
                            String.format(getApplicationContext().getString(R.string.toast_category_delete_used), usage),
                            Toast.LENGTH_SHORT).show();
                } else {
                    sqlCategoryHelper.removeCategory(id);
                    Log.i(TAG, "category deleted");
                    Toast.makeText(((Dialog) dialog).getOwnerActivity(), R.string.toast_category_deleted, Toast.LENGTH_SHORT).show();
                    fillList();
                }
            }
        });
        askToDeleteDialogFragment.setMessageId(R.string.dialog_delete_question_category);
        askToDeleteDialogFragment.show(getFragmentManager(), "category record");
    }

}
