package com.haluzpav.walletier.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.DatePicker;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.haluzpav.walletier.R;
import com.haluzpav.walletier.data.Record;
import com.haluzpav.walletier.sql.SQLCategoryHelper;
import com.haluzpav.walletier.sql.SQLRecordHelper;
import com.haluzpav.walletier.misc.ValuesHelper;

import java.util.Calendar;

/**
 * Activity for managing single record
 */
abstract class RecordActivity extends Activity {

    static final String TAG = "RecordActivity";

    SQLRecordHelper sqlRecordHelper;
    private SQLCategoryHelper sqlCategoryHelper;

    Record record;

    private TextView amountView;
    private Switch switchView;
    private Spinner spinner;
    private DatePicker datePicker;
    private TimePicker timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        sqlRecordHelper = new SQLRecordHelper(this);
        sqlCategoryHelper = new SQLCategoryHelper(this);

        initViews();

        timePicker.setIs24HourView(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fillSpinner();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_save_record:
                if (confirm() > 0) {
                    finish();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        amountView = (TextView) findViewById(R.id.amount);
        switchView = (Switch) findViewById(R.id.is_income);
        spinner = (Spinner) findViewById(R.id.spinner_categories);
        datePicker = (DatePicker) findViewById(R.id.date);
        timePicker = (TimePicker) findViewById(R.id.time);
    }

    /**
     * Fills Views with values from given record
     */
    void fillViews(Record record) {
        Log.i(TAG, "filling Views");
        amountView.setText(Double.toString(Math.abs(record.getAmount())));
        switchView.setChecked(record.getAmount() >= 0);
        setSpinnerValue(record.getReferenceCategory());

        Calendar recordCalendar = ValuesHelper.getCalendar(record.getDatetime());
        datePicker.updateDate(
                recordCalendar.get(Calendar.YEAR),
                recordCalendar.get(Calendar.MONTH),
                recordCalendar.get(Calendar.DAY_OF_MONTH)
        );
        timePicker.setCurrentHour(recordCalendar.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(recordCalendar.get(Calendar.MINUTE));
    }

    /**
     * Fills Spinner with categories
     */
    private void fillSpinner() {
        Log.i(TAG, "filling Spinner");
        Spinner spinner = (Spinner) findViewById(R.id.spinner_categories);
        Cursor cursor = sqlCategoryHelper.getAll();
        int[] textViews = new int[]{
                0,
                android.R.id.text1
        };
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                this, android.R.layout.simple_spinner_item, cursor, cursor.getColumnNames(), textViews);
        cursorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(cursorAdapter);
    }

    /**
     * Sets Spinner to item with given id. It must be done this (messy) way because
     * Spinner can be set only to item by its position and not by its id.
     * @param id id of item which is desired to be selected
     */
    private void setSpinnerValue(long id) {
        Adapter adapter = spinner.getAdapter();
        for (int i = 0; i < spinner.getCount(); i++) {
            if (adapter.getItemId(i) == id) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    /**
     * Gets values from Views and puts them into Record, which can be send to be saved in database
     * @return true if getting of values was successfull, otherwise false
     */
    boolean fillRecord() {
        Log.i(TAG, "filling Record");

        double amount;
        try {
            amount = Double.parseDouble(amountView.getText().toString());
        } catch (NumberFormatException e) {
            Log.e(TAG, "bad format of amount");
            Toast.makeText(this, R.string.toast_bad_format_amount, Toast.LENGTH_SHORT).show();
            return false;
        }
        amount *= switchView.isChecked() ? 1 : -1;

        long catId = spinner.getSelectedItemId();
        if (catId <= 0) {
            Log.e(TAG, "no category selected");
            Toast.makeText(this, R.string.toast_no_category_selected, Toast.LENGTH_SHORT).show();
            return false;
        }

        long datetime = ValuesHelper.getUnixTime(
                datePicker.getYear(),
                datePicker.getMonth(),
                datePicker.getDayOfMonth(),
                timePicker.getCurrentHour(),
                timePicker.getCurrentMinute(),
                0
        );

        record.setAmount(amount);
        record.setReferenceCategory(catId);
        record.setDatetime(datetime);

        return true;
    }

    /**
     * Should define what happens when confirmation button is clicked
     * @return number specific to the definition or negative number if something went wrong
     */
    abstract protected long confirm();

}
