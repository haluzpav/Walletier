package com.haluzpav.walletier.misc;

import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * This is used to alter values in Adapter used in ListView so they are in right format.
 */
public class MyViewBinder implements SimpleCursorAdapter.ViewBinder {

    @Override
    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        if (cursor.getColumnName(columnIndex).startsWith("datetime")) {
            long unixDate = cursor.getLong(columnIndex);
            TextView textView = (TextView) view;
            textView.setText(ValuesHelper.formatUnix(unixDate));
            return true;
        } else if (cursor.getColumnName(columnIndex).equals("amount")) {
            double amount = cursor.getDouble(columnIndex);
            TextView textView = (TextView) view;
            textView.setText(String.format(ValuesHelper.PRICE_FORMAT, amount));
            return true;
        } else {
            return false;
        }
    }

}