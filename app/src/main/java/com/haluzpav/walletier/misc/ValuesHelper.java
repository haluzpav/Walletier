package com.haluzpav.walletier.misc;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A bit messy class used to store some values or format them.
 */
public abstract class ValuesHelper {

    public static final String PRICE_FORMAT = "%+,.2f";
    public static final long SECONDS_30_DAYS = 2592000;

    /**
     * Convert unix time to human-readable date.
     * @param unix unix time in seconds
     * @return String with date and time formated by local standarts
     */
    public static String formatUnix(long unix) {
        return DateFormat.getDateTimeInstance().format(new Date(unix * 1000));
    }

    /**
     * Converts date and time to unix time
     * @param year year
     * @param month mothn
     * @param day day
     * @param hour hour
     * @param minute minute
     * @param second second
     * @return unix time in seconds
     */
    public static long getUnixTime(int year, int month, int day, int hour, int minute, int second) {
        Calendar calendar = new GregorianCalendar(year, month, day, hour, minute, second);
        return calendar.getTimeInMillis() / 1000;
    }

    /**
     * Converts unix time to Calendar, from which can be later gotten values like year, month etc.
     * @param unix unix time in seconds
     * @return Calendar with values based on given unix time
     */
    public static Calendar getCalendar(long unix) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(unix * 1000);
        return calendar;
    }

    public static final String EXTRA_RECORD_ID = "com.haluzpav.walletier.recordId";

}
