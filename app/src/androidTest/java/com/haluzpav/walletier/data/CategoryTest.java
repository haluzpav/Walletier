package com.haluzpav.walletier.data;

import junit.framework.TestCase;

import java.util.Random;

public class CategoryTest extends TestCase {

    private Random random;

    public void setUp() throws Exception {
        super.setUp();
        random = new Random();
    }

    public void testSetName() {
        Category cat = new Category();
        String name = String.valueOf(random.nextLong());

        cat.setName(name);

        assertEquals("get/set", name, cat.getName());
    }

    public void testInitEmpty() {
        Category cat = new Category();

        assertEquals("should be null", cat.getName(), null);
        assertEquals("should be -1", cat.getId(), -1);
        assertEquals("should be -1", cat.getDatetimeEditted(), -1);
        assertEquals("should be -1", cat.getDatetimeCreated(), -1);
    }

    public void testInitAll() {
        long id = random.nextLong();
        long dtc = random.nextLong();
        long dte = random.nextLong();
        String name = String.valueOf(random.nextLong());

        Category cat = new Category(id, name, dtc, dte);

        assertEquals("init/get", cat.getId(), id);
        assertEquals("init/get", cat.getName(), name);
        assertEquals("init/get", cat.getDatetimeCreated(), dtc);
        assertEquals("init/get", cat.getDatetimeEditted(), dte);
    }

}