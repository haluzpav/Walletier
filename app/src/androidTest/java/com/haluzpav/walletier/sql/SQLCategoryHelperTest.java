package com.haluzpav.walletier.sql;

import android.database.Cursor;
import android.test.AndroidTestCase;

import com.haluzpav.walletier.data.Category;
import com.haluzpav.walletier.data.Record;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * Tests working with "category" table in database. It uses currently present database in device
 * if there is one. Tests should clean after themselves.
 */
public class SQLCategoryHelperTest extends AndroidTestCase {

    private Random random;
    private SQLCategoryHelper sqlCategoryHelper;

    private static final int DELAY = 2;
    private static final int TRESH = 1;

    public void setUp() throws Exception {
        super.setUp();
        random = new Random();
        sqlCategoryHelper = new SQLCategoryHelper(getContext());
    }

    private long getCurrentTime() {
        return (new GregorianCalendar()).getTimeInMillis() / 1000;
    }

    private Category getNewRandomCategory() {
        Category cat = new Category();
        cat.setName(String.valueOf(random.nextInt()));
        return cat;
    }

    public void testAddCategory() {
        Category input = getNewRandomCategory();
        long id = sqlCategoryHelper.addCategory(input);
        Category output = sqlCategoryHelper.getCategory(id);

        assertEquals("add in/out id", id, output.getId());
        assertEquals("add in/out name", input.getName(), output.getName());
        assertTrue("add in/out creationTime", Math.abs(output.getDatetimeCreated() - getCurrentTime()) < TRESH);
        assertTrue("add in/out edittionTime", Math.abs(output.getDatetimeEditted() - getCurrentTime()) < TRESH);

        sqlCategoryHelper.removeCategory(id);
    }

    public void testAddEmptyCategory() {
        Category input = new Category();
        assertEquals("addEmpty not failed", sqlCategoryHelper.addCategory(input), -1);
    }

    /**
     * Among other things tests also changing time of editting. Thus sleep method is needed so
     * time of editting should be different than time of creation.
     */
    public void testEditCategory() {
        Category inputOrig = getNewRandomCategory();
        long id = sqlCategoryHelper.addCategory(inputOrig);

        try {
            Thread.sleep(DELAY * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Category outputOrig = sqlCategoryHelper.getCategory(id);
        outputOrig.setName(String.valueOf(random.nextInt()));
        long editted = sqlCategoryHelper.editCategory(outputOrig);

        assertEquals("edit number", editted, 1);

        Category outputChanged = sqlCategoryHelper.getCategory(id);

        assertEquals("edit in/out id", outputChanged.getId(), id);
        assertEquals("edit in/out name", outputChanged.getName(), outputOrig.getName());
        assertEquals("edit in/out creationTime", outputOrig.getDatetimeCreated(), outputChanged.getDatetimeCreated());
        assertTrue("edit in/out edittionTime", Math.abs(outputChanged.getDatetimeEditted() - getCurrentTime()) < TRESH);

        sqlCategoryHelper.removeCategory(id);
    }

    public void testEditToEmptyCategory() {
        Category inputOrig = getNewRandomCategory();
        long id = sqlCategoryHelper.addCategory(inputOrig);

        Category outputOrig = sqlCategoryHelper.getCategory(id);
        outputOrig.setName(null);
        try {
            sqlCategoryHelper.editCategory(outputOrig);
            fail("editToEmpty not failed");
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            assertTrue("editToEmpty wrong exception message", e.getMessage().contains("may not be NULL"));
        } catch (Exception e) {
            fail("editToEmpty wrong exception");
        }

        sqlCategoryHelper.removeCategory(id);
    }

    public void testRemoveCategory() {
        Category input = getNewRandomCategory();
        long id = sqlCategoryHelper.addCategory(input);
        long removed = sqlCategoryHelper.removeCategory(id);
        assertEquals("remove number", removed, 1);

        Category output = sqlCategoryHelper.getCategory(id);
        assertNull("remove out exists", output);
    }

    public void testGetAll() {
        final int toAdd = Math.abs(random.nextInt() % 10) + 1;
        ArrayList<Category> catsToAdd = new ArrayList<>(toAdd);
        ArrayList<Long> catIdsToAdd = new ArrayList<>(toAdd);
        for (int i = 0; i < toAdd; i++) {
            Category newCat = getNewRandomCategory();
            catsToAdd.add(newCat);
            catIdsToAdd.add(sqlCategoryHelper.addCategory(newCat));
        }
        Cursor cursor = sqlCategoryHelper.getAll();
        assertTrue("getAll empty", cursor.moveToFirst());
        boolean go = true;
        int tested = -1;
        while (go) {
            if (cursor.getLong(0) == catIdsToAdd.get(0)) {
                tested = 0;
            }
            if (tested >= 0) {
                assertEquals("getAll wrong id", catIdsToAdd.get(tested).longValue(), cursor.getLong(0));
                assertEquals("getAll worng name", catsToAdd.get(tested).getName(), cursor.getString(1));
                tested++;
            }
            go = cursor.move(1);
        }
        assertEquals("getAll not enough", tested, toAdd);
        for (int i = 0; i < toAdd; i++) {
            sqlCategoryHelper.removeCategory(catIdsToAdd.get(i));
        }
    }

    public void testGetCount() {
        Category catIn = getNewRandomCategory();
        long catId = sqlCategoryHelper.addCategory(catIn);

        final int toAdd = Math.abs(random.nextInt() % 10);
        SQLRecordHelper sqlRecordHelper = new SQLRecordHelper(getContext());
        ArrayList<Long> recIds = new ArrayList<>(toAdd);
        for (int i = 0; i < toAdd; i++) {
            Record rec = new Record();
            rec.setAmount(random.nextInt() + random.nextFloat());
            rec.setDatetime(random.nextInt());
            rec.setReferenceCategory(catId);
            recIds.add(sqlRecordHelper.addRecord(rec));
        }

        assertEquals("getCount wrong usage number", sqlCategoryHelper.getCount(catId), toAdd);

        for (long i : recIds) {
            sqlRecordHelper.removeRecord(i);
        }
        sqlCategoryHelper.removeCategory(catId);
    }

    public void testGetColumns() {
        String[] columns = sqlCategoryHelper.getColumns();
        assertNotNull("getColumns null", columns);
        for (String column : columns) {
            assertNotNull("getColumns column null", column);
        }
    }

    public void testGetAllCommand() {
        assertNotNull("getAllCommand null", sqlCategoryHelper.getAllCommand());
    }
}